$(function () {
    $('.btn-chevron').click(function () {
        if(!$(this).hasClass('btn-collapsed')) {
            $(this).removeClass('btn-active').addClass('btn-secondary btn-collapsed');
            $(this).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up fa-inverse');
        } else {
            $(this).addClass('btn-active').removeClass('btn-secondary btn-collapsed');
            $(this).find('i').removeClass('fa-chevron-up fa-inverse').addClass('fa-chevron-down');
        }
    })
})