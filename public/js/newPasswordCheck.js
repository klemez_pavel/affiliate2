$(function () {
    $('#newAccountPassword').change(function () {
        if(this.value.length != 0) {
            $('#oldAccountPassword').prop('required', true);
        } else {
            $('#oldAccountPassword').prop('required', false);
        }
    })
    $('#oldAccountPassword').change(function () {
        if(this.value.length != 0) {
            $('#newAccountPassword').prop('required', true);
        } else {
            $('#newAccountPassword').prop('required', false);
        }
    })
})
