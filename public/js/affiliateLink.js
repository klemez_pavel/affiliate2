$(function () {
    $('#linkGenerateForm').submit(function (e) {
        e.preventDefault();
        $.ajax(
            {
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/account/linkgeneration',
                data: {
                    affiliate_link: $('#affiliate_link').val()
                }
            }
        ).done(function (html) {
            $('#affiliate_link_utm').val("").val(html.link).select();
            if ($('#generateError').length) {
                $('#generateError').remove();
            }
        }).fail(function (error) {
            var obj = $.parseJSON(error.responseText);
            if ($('#generateError').length) {
                $('#generateError').remove();
            }
            $('#linkGenerateForm')
                .after('<div id="generateError" class="alert alert-danger" style="margin-top: 5px"> '
                    + obj.errors + ' </div>');
        })
    })
})
