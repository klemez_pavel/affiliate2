<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/**
 * @controller SiteController
 */
Route::get('/', 'SiteController@index');
Route::get('/home', 'SiteController@home');
/**
 * @controller LoginController
 */

Route::post('/account/login', 'LoginController@login');
Route::get('/account/logout', 'LoginController@logout');
/**
 * @controller RegisterController
 */
Route::get('/account/registration', 'RegisterController@create');
Route::post('/account/registration', 'RegisterController@registration');
/**
 * @controller AccountController
 */
Route::get('/account/editpartnerdata', 'AccountController@showEdit');
Route::post('/account/editpartnerdata', 'AccountController@editPartnerData');
Route::get('/account/info', 'AccountController@info');
Route::post('/account/linkgeneration', 'AccountController@linkGeneration');

/**
 * @controller InfoController
 */
Route::get('/info/bookkeeping','InfoController@bookkeeping');
Route::get('/info/orderstatistics', 'InfoController@orderStatistics');
Route::get('/info/promotionalmaterials', 'InfoController@promoMaterials');
Route::get('/generalstatistics', 'InfoController@generalStatistics');
