<?php

namespace App\Services;

use App\Contracts\AccountInterface;
use GuzzleHttp\Client;

class AccountService implements AccountInterface
{
    private $httpClient;

    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param $email
     * @param $password
     * @return mixed
     */
    public function login($email, $password)
    {
        $response = $this->httpClient->request('POST', env('API_BASE_URI') . '/account/login', [
            'auth' => [
                env('API_USERNAME'),
                env('API_PASSWORD')
            ],
            'json' => [
                'email' => $email,
                'password' => $password,
            ],
        ])->getBody();
        return json_decode($response, true);
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function registration(array $params)
    {
        $response = $this->httpClient->request('POST', env('API_BASE_URI') . '/account/registration', [
            'auth' => [
                env('API_USERNAME'),
                env('API_PASSWORD')
            ],
            'body' => json_encode($params),
        ])->getBody();
        return json_decode($response, true);
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function editPartnerData(array $params)
    {
        $response = $this->httpClient->request('POST',  env('API_BASE_URI') . '/account/editpartnerdata', [
            'auth' => [
                env('API_USERNAME'),
                env('API_PASSWORD')
            ],
            'body' => json_encode($params)
        ])->getBody();
        return json_decode($response, true);
    }

    /**
     * @param $idsession
     * @return mixed
     */
    public function info($idsession)
    {
        $response = $this->httpClient->request('POST',  env('API_BASE_URI') . '/account/info', [
            'auth' => [
                env('API_USERNAME'),
                env('API_PASSWORD')
            ],
            'json' => [
                'idsession' => $idsession,
            ]
        ])->getBody();
        return json_decode($response,true);
    }

    public function partnerBalance($idsession)
    {
        $response = $this->httpClient->request('POST',  env('API_BASE_URI') . '/account/info', [
            'auth' => [
                env('API_USERNAME'),
                env('API_PASSWORD')
            ],
            'json' => [
                'idsession' => $idsession,
            ]
        ])->getBody();
        return json_decode($response,true)['balance'];
    }


    /**
     * @param $idsession
     * @param $link
     * @return mixed
     */
    public function linkGeneration($idsession, $link)
    {
        $response = $this->httpClient->request('POST',  env('API_BASE_URI') . '/account/linkgeneration', [
            'auth' => [
                env('API_USERNAME'),
                env('API_PASSWORD')
            ],
            'json' => [
                'idsession' => $idsession,
                'affiliatelink' => $link,
            ]
        ])->getBody();
        return json_decode($response, true);
    }
}