<?php

namespace App\Services;

use App\Contracts\InfoInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Created by PhpStorm.
 * User: kleme
 * Date: 14.09.2017
 * Time: 16:12
 */
class InfoService implements InfoInterface
{
    private $httpClient;

    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param $idsession
     * @return mixed
     */
    public function bookkeeping($idsession)
    {
        $response = $this->httpClient->request('POST', env('API_BASE_URI') . '/info/bookkeeping', [
            'auth' => [
                env('API_USERNAME'),
                env('API_PASSWORD')
            ],
            'json' => [
                'idsession' => $idsession,
            ]
        ])->getBody();
        return json_decode($response, true);
    }

    /**
     * @param string $idsession
     * @return mixed
     */
    public function promotionalMaterials($idsession)
    {
        $response = $this->httpClient->request('POST',  env('API_BASE_URI') . '/info/promotionalmaterials', [
            'auth' => [
                env('API_USERNAME'),
                env('API_PASSWORD')
            ],
            'json' => [
                'idsession' => $idsession,
            ]
        ])->getBody();
        return json_decode($response, true);
    }

    /**
     * @param string $idsession
     * @return mixed
     */
    public function orderStatistics($idsession)
    {
        $response = $this->httpClient->request('POST',  env('API_BASE_URI') . '/info/orderstatistics', [
            'auth' => [
                env('API_USERNAME'),
                env('API_PASSWORD')
            ],
            'json' => [
                'idsession' => $idsession,
            ]
        ])->getBody();
        return json_decode($response, true);
    }

    /**
     * @return mixed
     */
    public function generalStatistics()
    {
        $response = $this->httpClient->request('GET',  env('API_BASE_URI') . '/generalstatistics', [
            'auth' => [
                env('API_USERNAME'),
                env('API_PASSWORD')
            ],
        ])->getBody();
        return json_decode($response, true);
    }
}