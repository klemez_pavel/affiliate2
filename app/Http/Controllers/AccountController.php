<?php

namespace App\Http\Controllers;

use App\Contracts\AccountInterface;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    private $account;

    public function __construct(AccountInterface $account)
    {
        $this->account = $account;
        $this->middleware('idsession')->except('index', 'login', 'registration');
    }

    public function showEdit(Request $request)
    {
        $data = $this->account->info($request->session()->get('idsession'));
        return view('account.editPartnerData', ['user' => $data]);
    }

    public function editPartnerData(Request $request)
    {
        try {
            $params = $request->except('_token');
            $params['idsession'] = $request->session()->get('idsession');
            $data = $this->account->editPartnerData($params);
            $request->session()->put('idpartner', $data['idpartner']);
            return redirect('account/info');
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $responseBody = $e->getResponse()->getBody();
                $errors = $this->getResponseErrors($responseBody, $e);
            }
            return back()->withErrors(['errors' => $errors ?? null]);
        }
        // @todo make method edit
    }

    public function info(Request $request)
    {
        $idsession = $request->session()->get('idsession');
        try {
            $data = $this->account->info($idsession);
            return view('account.info', ['user' => $data]);
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $responseBody = $e->getResponse()->getBody();
                $errors = $this->getResponseErrors($responseBody, $e);
            }
            return view('account.info')->withErrors(['errors' => $errors ?? null]);
        }
        // @todo make info method
    }

    public function linkGeneration(Request $request)
    {
        $idsession = $request->session()->get('idsession');
        $link = $request->input('affiliate_link');

        if ($position = strpos($link, '?')) {
            $link = substr($link, 0, $position);
        }
        try {
            $data = $this->account->linkGeneration($idsession, $link);
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $responseBody = $e->getResponse()->getBody();
                $errors = $this->getResponseErrors($responseBody, $e);
            }
            return response()->json([
                'errors' => $errors ?? null
            ], $e->getCode(),[], JSON_UNESCAPED_UNICODE);
        }
        return response()->json([
            'link' => $data['affiliate_link_utm'],
        ], 200, [], JSON_UNESCAPED_UNICODE);
        // @todo make link generation
    }
}
