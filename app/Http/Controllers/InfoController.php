<?php

namespace App\Http\Controllers;

use App\Contracts\InfoInterface;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;

class InfoController extends Controller
{
    private $info;

    public function __construct(InfoInterface $info)
    {
        $this->info = $info;
        $this->middleware('idsession');
    }

    public function bookkeeping(Request $request)
    {
        try {
            $idsession = $request->session()->get('idsession');
            $data = $this->info->bookkeeping($idsession);
        } catch (RequestException $e) {
            $responseBody = $e->getResponse()->getBody();
            $errors = $this->getResponseErrors($responseBody, $e);
        }
        return view('info.bookkeeping', ['data' => $data, 'errors' => $errors ?? null]);
    }

    public function orderStatistics(Request $request)
    {
        try {
            $idsession = $request->session()->get('idsession');
            $data = $this->info->orderStatistics($idsession);
        } catch (RequestException $e) {
            $responseBody = $e->getResponse()->getBody();
            $errors = $this->getResponseErrors($responseBody, $e);
        }
        return view('info.orderStatistics', ['data' => $data, 'error' => $errors ?? null]);
    }

    public function promoMaterials(Request $request)
    {
        try {
            $idsession = $request->session()->get('idsession');
            $data = $this->info->promotionalMaterials($idsession);
        } catch (RequestException $e) {
            $responseBody = $e->getResponse()->getBody();
            $errors = $this->getResponseErrors($responseBody, $e);
        }
        return view('info.promotionalMaterials', ['data' => $data, 'error' => $errors ?? null]);
    }
}