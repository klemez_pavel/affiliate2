<?php

namespace App\Http\Controllers;

use App\Contracts\AccountInterface;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    private $account;

    public function __construct(AccountInterface $account)
    {
        $this->account = $account;
        $this->middleware('guest');
    }

    public function create()
    {
        return view('account.registration');
    }

    public function registration(Request $request)
    {
        try {
            $this->account->registration($request->except('_token'));
            return redirect('/')->with('thanksView', 'true');
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $responseBody = $e->getResponse()->getBody();
                $errors = $this->getResponseErrors($responseBody, $e);
            }
        }
        return back()->withErrors(['errors' => $errors ?? null]);
        // @todo make method register
    }


}
