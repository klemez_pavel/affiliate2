<?php

namespace App\Http\Controllers;

use App\Contracts\AccountInterface;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    private $account;

    public function __construct(AccountInterface $account)
    {
        $this->account = $account;
        $this->middleware('idsession')->except('login', 'logout');
    }


    public function login(Request $request)
    {
        try {
            $data = $this->account->login(
                $request->input('email'),
                $request->input('password')
            );
            session()->put('idsession', $data['idsession']);
            session()->put('idpartner', $data['idpartner']);
            return redirect('home');
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $responseBody = $e->getResponse()->getBody();
                $errors = $this->getResponseErrors($responseBody, $e);
            }
        }
        return back()->withErrors(['errors' => $errors ?? null]);
    }

    public function logout(Request $request){
        $request->session()->forget(['idsession', 'idpartner']);
        return redirect('/');
    }
}
