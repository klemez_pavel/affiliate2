<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;



    protected function getResponseErrors($responseBody, RequestException $e)
    {
        $decoded = json_decode($responseBody);
        foreach ($decoded->errors as $error) {
            $requestErrors[] = $error;
        }
        return $requestErrors ?? null;
    }
}
