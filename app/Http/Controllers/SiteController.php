<?php
/**
 * Created by PhpStorm.
 * User: kleme
 * Date: 16.10.2017
 * Time: 12:33
 */

namespace App\Http\Controllers;

use App\Contracts\InfoInterface;
use GuzzleHttp\Exception\RequestException;

class SiteController extends Controller
{
    private $info;

    public function __construct(InfoInterface $info)
    {
        $this->info = $info;
        $this->middleware('idsession')->except('index');
    }

    public function index()
    {
        if(session()->exists('idsession')) return redirect('home');
        return view('index');
    }

    public function home()
    {
        try {
            $data = $this->info->generalStatistics();
        } catch (RequestException $e) {
            $responseBody = $e->getResponse()->getBody();
            $errors = $this->getResponseErrors($responseBody, $e);
        }
        return view('home', ['data' => $data, 'errors' => $errors ?? null]);
    }

}