<div class="row">
    <div class="col-lg-12">
        <h6 class="mb-4 mt-4">Для подачи заявки на участие в партнерской программе, пожалуйста заполните все поля
            ниже:</h6>
        <form class="form-style form-registration" method="POST" action="/account/registration">
            <div class="row">
                {{ csrf_field() }}
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="surname" class="col-form-label">Фамилия</label>
                        <input type="text" class="form-control" id="surname" name="surname" required>
                    </div>
                    <div class="form-group">
                        <label for="city" class="col-form-label">Город</label>
                        <input type="text" class="form-control" id="city" name="city" required>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-form-label">Пароль</label>
                        <input type="password" class="form-control" id="password" name="password" required>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="name" class="col-form-label">Имя</label>
                        <input type="text" class="form-control" id="name" name="name" required>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-form-label">Email</label>
                        <input type="text" class="form-control" id="email" name="email" required>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-form-label">Подтверждение пароля</label>
                        <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" required>
                    </div>
                    <div class="text-center mt-5">
                        <button type="submit" class="btn btn-primary">Зарегистрироваться</button>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="patronymic" class="col-form-label">Отчество</label>
                        <input type="text" class="form-control" id="patronymic" name="patronymic" required>
                    </div>
                    <div class="form-group">
                        <label for="site" class="col-form-label">Сайт</label>
                        <input type="text" class="form-control" id="site" name="site" required>
                    </div>
                </div>
            </div>
        </form>
        @include('layouts.errors')
    </div>
</div>
