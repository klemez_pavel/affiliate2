<div class="col-md-6 mx-auto p-3 mt-5 handshake icon-check" id="thanksDiv">
    <div class="m-3">
        <img src="img/handshake.png" class="mx-auto d-block" alt="">
    </div>
    <div>
        <p class="text-center" style="font-size: 24px;"><b>Спасибо за регистрацию!</b></p>
    </div>
    <div class="text-center">
        <button class="btn btn-primary text-center" id="loginRefButton">Войти</button>
    </div>

</div>