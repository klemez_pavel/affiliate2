<div class="row">
    <div class="col-lg-12">
        <div style="{{session('thanksView') ? "display:none;" : ""}}" id="loginForm">
            <form class="form-style form-login" method="POST" action="/account/login">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="email" class="col-form-label">Email</label>
                    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp"
                           placeholder="" required>
                </div>
                <div class="form-group">
                    <label for="password" class="col-form-label">Пароль</label>
                    <input type="password" class="form-control" id="password" name="password" required>
                </div>
                <div class="text-center mt-4">
                    <button type="submit" class="btn btn-primary">Войти</button>
                </div>
                @include('layouts.errors')
            </form>
        </div>
    </div>
</div>


