@extends('layouts.master')
@section('content')
    <div class="main-body m-2">
        <div class="account-view">
            <div class="row justify-content-md-center no-border-bottom">
                <div class="col-lg-9">
                    <div class="row justify-content-md-center">
                        <div class="col-lg-6 label">Email</div>
                        <div class="col-lg-6">{{ $user['email'] }}</div>
                    </div>
                    <div class="row justify-content-md-center">
                        <div class="col-lg-6 label">Фамилия</div>
                        <div class="col-lg-6">{{ $user['surname'] }}</div>
                    </div>
                    <div class="row justify-content-md-center">
                        <div class="col-lg-6 label">Имя</div>
                        <div class="col-lg-6">{{ $user['name'] }}</div>
                    </div>
                    <div class="row justify-content-md-center">
                        <div class="col-lg-6 label">Отчество</div>
                        <div class="col-lg-6">{{ $user['patronymic'] }}</div>
                    </div>
                    <div class="row justify-content-md-center">
                        <div class="col-lg-6 label">Город</div>
                        <div class="col-lg-6">{{ $user['city'] }}</div>
                    </div>
                    <div class="row justify-content-md-center">
                        <div class="col-lg-6 label">Сайт</div>
                        <div class="col-lg-6">
                            <a href="#">{{ $user['site'] }}</a>
                        </div>
                    </div>
                    <div class="row justify-content-md-center no-border-bottom">
                        <div class="col-lg-6 label">Способ вывода средств *</div>
                        <div class="col-lg-6">{{ $user['card'] }}</div>
                        <div class="row no-border-bottom">
                            <div class="col-lg-12">
                                <p class="account-view-description">* Для изменения способа вывод средств,
                                    пожалуйста обращайтесь по электронной почте <a
                                            href="mailto:partner@grandstock.ru">partner@grandstock.ru</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-md-center no-border-bottom">
                        <div class="col-lg-12 text-center">
                            <a href="/account/editpartnerdata">
                                <button type="button" class="btn btn-primary mt-3">Редактировать</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection