@extends('layouts.master')

@section('content')

    <div class="main-body m-2">
        <form method="POST" action="/account/editpartnerdata">
            {{ csrf_field() }}

            <div class="form-group row">
                <label for="email" class="col-sm-4 col-form-label">Email</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-sm" id="email" name="email"
                           value="{{ $user['email'] }}" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="newPassword" class="col-sm-4 col-form-label">Пароль</label>
                <div class="col-sm-8">
                    <input type="password" class="form-control form-control-sm" id="newAccountPassword" name="password">
                </div>
            </div>
            <div class="form-group row">
                <label for="oldPassword" class="col-sm-4 col-form-label">Старый пароль</label>
                <div class="col-sm-8">
                    <input type="password" class="form-control form-control-sm" id="oldAccountPassword"
                           name="oldPassword">
                </div>
            </div>
            <div class="form-group row">
                <label for="surname" class="col-sm-4 col-form-label">Фамилия</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-sm" id="surname" name="surname"
                           value="{{ $user['surname'] }}" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="name" class="col-sm-4 col-form-label">Имя</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-sm" id="name" name="name"
                           value="{{ $user['name'] }}" required></div>
            </div>

            <div class="form-group row">
                <label for="patronymic" class="col-sm-4 col-form-label">Отчество</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-sm" id="patronymic" name="patronymic"
                           value="{{ $user['patronymic'] }}" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="city" class="col-sm-4 col-form-label">Город</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-sm" id="city" name="city"
                           value="{{ $user['city'] }}" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="site" class="col-sm-4 col-form-label">Сайт</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-sm" id="site" name="site"
                           value="{{ $user['site']}}" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="site" class="col-sm-4 col-form-label">Способ вывода средств *</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-sm" id="site" name="site"
                           readonly value=" {{ $user['card']}}">
                    <p class="text-right account-view-description">* Для изменения способа вывод средств, пожалуйста
                        обращайтесь по
                        электронной почте <a href="mailto:partner@grandstock.ru">partner@grandstock.ru</a></p>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-8 ">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </div>
        </form>
    </div>
    @include('layouts.errors')
@endsection