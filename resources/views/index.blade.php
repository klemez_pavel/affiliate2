<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tilte</title>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/styles.css">
</head>
<body>
<div class="container">
    @include('layouts.login-navs')
    <div class="row no-gutters">
        <div class="col-lg-12 nav-border-bottom">
            <ul class="nav nav-pills justify-content-center p-2" id="authTab" role="tablist">
                <li class="nav-item">
                    <a href="#login" class="custom-link {{ session('thanksView') ? "" : "custom-active" }}" id="login-tab"
                       data-toggle="tab" aria-controls="login">Войти</a>
                </li>
                <li class="nav-item">
                    <a href="#register" class="custom-link" id="register-tab" data-toggle="tab" aria-controls="register">Зарегистрироваться</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="tab-content">
        <div class="tab-pane fade show active no-border" id="login" role="tabpanel" aria-labelledby="login-tab">
            @include('account.login')
        </div>
        <div class="tab-pane fade no-border" id="register" role="tabpanel" aria-labelledby="register-tab">
            @include('account.registration')
        </div>
    </div>

    @if(session('thanksView'))
        @include('account.thanksForRegistration')
    @endif
</div>
@include('layouts.footer')
<script src="/js/nav-unlock.js"></script>
</body>

</html>