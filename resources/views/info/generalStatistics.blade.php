@section('content')
        @if($data['statistics'])
            <div class="main-body m-2">
                <table class="table table-striped table-sm table-no-td-borders table-tr-margin">
                    <thead>
                    <tr>
                        <th scope="row">#</th>
                        <th class="text-left">ФИО</th>
                        <th class="text-center">Сумма заказов</th>
                        <th class="text-center">Сумма вознаграждения</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($data['statistics'] as $statistic)
                        <tr>
                            <td scope="row" class="td-left-radius">{{ $loop->iteration }}</td>
                            <td class="text-left">{{ $statistic['fio'] }}</td>
                            <td class="text-center">{{ $statistic['sum_of_sales'] }}</td>
                            <td class="text-center td-right-radius">{{ $statistic['amount_of_remuneration'] }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    @include('layouts.errors')
@endsection