@extends('layouts.master')

@section('content')

    <div class="main-body m-2">
        <table class="table table-sm table-no-td-borders table-tr-margin">
            <thead>
            <tr>
                <th class="text-center">Дата заказа</th>
                <th class="text-center">Номер заказа</th>
                <th class="text-center">Сумма заказа</th>
                <th class="text-center">Сумма вознаграждения</th>
                <th class="text-center">Статус заказа</th>
                <th class="text-center">Причина отказа</th>
                <th class="text-center">Новый клиент</th>
                <th class="text-center">Название программы</th>
                <th class="no-padding no-width"></th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data['orders'] as $order)
                <tr class="{{ $loop->iteration % 2 ? "table-striped-dark" : "table-striped-light" }}">
                    <td scope="row" class="td-left-radius">{{ date("Y-m-d H:i:s",strtotime($order['order_date'])) }}</td>
                    <td>{{ $order['order_number'] }}</td>
                    <td>{{ $order['sum_of_sale'] }}</td>
                    <td>{{ $order['amount_of_remuneration'] }}</td>
                    <td>{{ $order['order_status'] }}</td>
                    <td>{{ $order['rejection_reason'] }}</td>
                    <td>{{ $order['new_client'] ? 'Да' : 'Нет' }}</td>
                    <td>{{ $order['name_program'] }}</td>
                    <td class="no-padding no-width td-right-radius">
                        <div class="btn-cell">
                            <button class="btn btn-primary btn-block btn-chevron" data-toggle="collapse"
                               href="#collapse{{ $loop->iteration }}"
                               aria-expanded="false" aria-controls="collapse{{ $loop->iteration }}">
                                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                <tr class="collapse" id="collapse{{ $loop->iteration }}">
                    <td colspan="{{ count($order) }}" class="no-padding">
                        <div class="">
                            <table class="table">
                                <thead>
                                <th>Имя</th>
                                <th>Свойство</th>
                                <th>Количество</th>
                                <th>Цена</th>
                                <th>Сумма</th>
                                </thead>
                                <tbody>
                                @foreach($order['products'] as $product)
                                    <tr>
                                        <td>{{ $product['product'] }}</td>
                                        <td>{{ $product['product_property'] }}</td>
                                        <td> {{ $product['count'] }}</td>
                                        <td>{{ $product['price'] }}</td>
                                        <td> {{ $product['summ'] }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection