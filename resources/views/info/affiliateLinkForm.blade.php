<div class="main-body m-2">
    <h5 class="text-center mb-4">Создайте свою персональную ссылку:</h5>

    <form method="POST" id="linkGenerateForm">
        <div class="form-group">
            <label for="affiliate_link">Вставьте сюда URL товара или категории сайта для получения персональной
                ссылки:</label>
            <input type="text" class="form-control form-control-sm" id="affiliate_link" aria-describedby="emailHelp"
                   name="affiliate_link" required>
        </div>
        <button type="submit" class="btn btn-primary">Получить</button>
    </form>
    <div class="form-group mt-4">
        <label for="affiliate_link_utm">Ваша персональная ссылка:</label>
        <input type="text" class="form-control form-control-sm" id="affiliate_link_utm" readonly>
    </div>
</div>