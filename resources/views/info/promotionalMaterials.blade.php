@extends('layouts.master')

@section('content')

    @include('info.affiliateLinkForm')
    <div class="main-body m-2">
        <div class="row">
            @foreach ($data['promotionalmaterials'] as $promoMaterial)
                <div class="col-lg-4">
                    <div class="card rounded" style="width: 236px">

                        <div class="program-card-{{$loop->iteration}} program-card">
                            {{--<img class="card-img-top" src="http://via.placeholder.com/236x115" alt="Card image cap">--}}
                            <p class="card-text program-name text-center">Программа:
                                <br> {{ $promoMaterial['name_of_program'] }} </p>
                        </div>

                        <div class="card-body">
                            <p class="card-text text-center">{{ $promoMaterial['program_description'] }}</p>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item text-center">Комиссия за первый заказ
                                <b>{{ $promoMaterial['commission_for_first_order'] }}%</b></li>
                            <li class="list-group-item text-center">Комиссия за повторный заказ
                                <b>{{ $promoMaterial['commission_for_reordering'] }}%</b></li>
                            @if(strlen($promoMaterial['coupon_code']) != 0)
                                <li class="list-group-item text-center">
                                    <div class="text-uppercase">Код купона:</div>
                                    <div class="text-uppercase"><b>{{ $promoMaterial['coupon_code'] }}</b></div>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection