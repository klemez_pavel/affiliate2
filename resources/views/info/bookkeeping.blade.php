@extends('layouts.master')

@section('content')
    <!-- Add warning if data empty -->
    <div class="main-body m-2">
        <table class="table table-no-td-borders table-tr-margin">
            <thead>
            <tr>
                <th class="text-left">Дата</th>
                <th class="text-center">Сумма</th>
                <th class="text-center">Выплачено</th>
                <th class="text-center">Комментарий</th>
                <th class="no-padding no-width" style="width: 18px!important"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($data['payments'] as $payment)

                <tr class="{{ ($loop->iteration % 2) ? "table-striped-dark" : "table-striped-light" }}">
                    <td scope="row" class="td-left-radius">{{ date("Y-m-d H:i:s", strtotime($payment['date'])) }}</td>
                    <td class="text-center">{{ $payment['sum'] }}</td>
                    <td class="text-center">{{ $payment['paid'] ? "Да" : "Нет" }}</td>
                    <td class="text-cetner">{{ $payment['comment'] }}</td>
                    {{-- @todo uncomment when can make collapse normal--}}
                    <td class="no-padding no-width td-right-radius">
                        <div class="btn-cell">
                        <a class="btn btn-primary btn-block btn-chevron" data-toggle="collapse"
                           href="#collapse{{ $loop->iteration }}"
                           aria-expanded="false" aria-controls="collapse{{ $loop->iteration }}">
                            <i class="fa fa-chevron-down" aria-hidden="true"></i>
                        </a>
                        </div>
                    </td>
                </tr>
                <tr class="collapse" id="collapse{{ $loop->iteration }}">
                    <td colspan="{{ count($payment) }}" class="no-padding">
                        <div class="">
                            <table class="table">
                                <thead>
                                <th>Дата</th>
                                <th>Заказ</th>
                                <th>Комиссия</th>
                                </thead>
                                <tbody>
                                @foreach($payment['orders'] as $order)
                                    <tr>
                                        <td>{{ date("Y-m-d H:i:s",strtotime($order['order_date'])) }}</td>
                                        <td>{{ $order['order_number'] }}</td>
                                        <td>{{ $order['amount_of_remuneration'] }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    @include('layouts.errors')
@endsection
