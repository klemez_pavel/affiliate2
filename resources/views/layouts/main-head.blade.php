@inject('account', 'App\Services\AccountService')
<div class="main-head">
    <div class="row no-gutter">
        <div class="col-lg-12">
            <div class="main-title float-left">
                <!-- Add Switch? -->
                @if(Request::is("home"))
                    Топ {{ count($data['statistics']) }} партнеров
                @endif

                @if(Request::is("account/info") || Request::is("account/editpartnerdata"))
                    Информация об аккаунте
                @endif
                @if(Request::is("info/promotionalmaterials"))
                    Купоны и ссылки
                @endif
                @if(Request::is("info/bookkeeping"))
                    Выплаты
                @endif
                @if(Request::is("info/orderstatistics"))
                    Ваши партнерские заказы
                @endif
            </div>

            <div class="float-right">
                <button type="button" class="btn btn-outline-primary">
                    <a href="/account/logout">Выйти</a>
                </button>
            </div>
            @if(session()->has('idsession'))
                <div class="float-right main-balance">
                    Баланс: {{ $account->partnerBalance(session()->get('idsession'))  }}
                </div>
            @endif
        </div>
    </div>
</div>