<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tilte</title>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M"
          crossorigin="anonymous">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/styles.css">

</head>
<body>
<div class="container">
    <div class="row no-gutters">
        <div class="col-lg-2">

            @include('layouts.sidebar')
        </div>
        <div class="col-lg-10">

            @include('layouts.main-head')
            @yield('content')
        </div>
    </div>
</div>
@include('layouts.footer')
</body>

</html>