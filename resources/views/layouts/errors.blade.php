@if(count($errors))
    <div class="mt-3">
        <ul class="list-group">
            @foreach($errors->all() as $error)
                <li class="list-group-item text-center alert-font border-0">
                    <i class="fa fa-times-circle fa-lg" aria-hidden="true"></i>
                    <b> {{ $error }} </b></li>
            @endforeach
        </ul>
    </div>
@endif