@inject('account', 'App\Services\AccountService')


<div class="sidebar-head text-center">
    <h5>Личный кабинет</h5>
</div>

<ul class="list-group sidebar-menu">
    <li class="list-group-item li-sidebar-element  {{ Request::is("home") ? 'border-active' : ''}}">
        <a class="d-block img-stats a-sidebar-elem a-img-position "
           href="/home">
            <span class="align-text-bottom">Общая статистика</span>
        </a>
    </li>

    <li class="list-group-item li-sidebar-element  {{ Request::is("account/info") ? 'border-active' : ''}} ">
        <a class="d-block img-account a-sidebar-elem a-img-position"
           href="/account/info">
            <span class="align-text-bottom v-aligned">Аккаунт</span>
        </a>
    </li>

    <li class="list-group-item li-sidebar-element {{ Request::is("info/promotionalmaterials") ? 'border-active' : ''}}">
        <a class="d-block img-coupons a-sidebar-elem a-img-position"
           href="/info/promotionalmaterials">
            <span class="align-text-bottom">Купоны и ссылки</span>
        </a>
    </li>

    <li class="list-group-item  li-sidebar-element {{ Request::is("info/bookkeeping") ? 'border-active' : ''}}">
        <a class="d-block img-payments a-sidebar-elem a-img-position"
           href="/info/bookkeeping">
            <span class="align-text-bottom v-aligned">Выплаты</span>
        </a>
    </li>

    <li class="list-group-item  li-sidebar-element {{ Request::is("info/orderstatistics") ? 'border-active' : ''}}">
        <a class="d-block img-orders a-sidebar-elem a-img-position"
           href="/info/orderstatistics">
            <span class="align-text-bottom v-aligned">Заказы</span>
        </a>
    </li>

</ul>
