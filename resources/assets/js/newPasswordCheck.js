$(function () {
    $('#newPassword').change(function () {
        if(this.value.length != 0) {
            $('#oldPassword').prop('required', true);
        } else {
            $('#oldPassword').prop('required', false);
        }
    })
    $('#oldPassword').change(function () {
        if(this.value.length != 0) {
            $('#newPassword').prop('required', true);
        } else {
            $('#newPassword').prop('required', false);
        }
    })
})
