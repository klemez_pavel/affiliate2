$(function () {
    $('#registrationForm').on('submit', function () {
        if($('#registrationPassword').val() === $('#confirmPassword').val()) {
            $('#passwordValidate').hide();
            return true;
        }
        $('#passwordValidate').html('').append('Пароли не совпадают').show();
        return false;
    })
})
